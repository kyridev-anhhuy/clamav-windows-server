# Clamav Installation on Windows Server 2012 R2

Directory structure
* ./clamav-0.101.2.exe : installation script from Cisco Clavav project
* ./config/* : config files for clamd (daemon) and freshclam (script to download latest virus library)
* ./database : virus dictionary downloaded from upstream
* ./scripts: helper Powershell scripts to install clamd and start scan of "Document" directory

## Installation
* Run clamav-0.101.2.exe on target platfor
* Copy ./config/* to installed platform ( C:\Program Files\ClamAV )
* Copy ./database to installed platform
* cd to installed platform; copy clamd-svcs.ps1 and run from installed platform

## Post install
* Verify clamd running: Open powershell and run Get-Process 
* Test scan current user's Documents directory: run-clamav.bat

```
PS C:\Program Files\ClamAV> C:\"Program Files"\ClamAV\clamscan.exe -d C:\"Program Files"\ClamAV\database\ -l C:\"Program Files"\ClamAV\log\scan.log --recursive --exclude="[^\]*\.dbx$" --exclude="[^\]*\.tbb$" --exclude="[^\]*\.pst$" --exclude="[^\]*\.dat$" --exclude="[^\]*\.log$" --exclude="[^\]*\.chm$" -i C:\Users\kyr-aha\Documents

----------- SCAN SUMMARY -----------
Known viruses: 6152621
Engine version: 0.101.2
Scanned directories: 29
Scanned files: 578
Infected files: 0
Data scanned: 582.17 MB
Data read: 351.77 MB (ratio 1.65:1)
Time: 993.670 sec (16 m 33 s)

```
