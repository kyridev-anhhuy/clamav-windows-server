# Install
Invoke-Expression "C:\Users\kyr-aha\Downloads\ClamAV-Remote-Installation\clamav-0.101.2"


# move config and databases
move-item -path C:\Users\kyr-aha\Downloads\ClamAV-Remote-Installation\database\* -destination "C:\Program Files\ClamAV\database" -force –whatif
move-item -path C:\Users\kyr-aha\Downloads\ClamAV-Remote-Installation\database\* -destination "C:\Program Files\ClamAV\database" -force -confirm

# Same thing for configs
move-item -path  C:\Users\kyr-aha\Downloads\ClamAV-Remote-Installation\config\*.conf -destination "C:\Program Files\ClamAV\"  –whatif
move-item -path  C:\Users\kyr-aha\Downloads\ClamAV-Remote-Installation\config\*.conf -destination "C:\Program Files\ClamAV\" -confirm

# Get runassvc utility to fire up the service
Copy-Item -Path \\tsclient\C\Users\anhhuy.ha\Downloads\ClamAV\ClamAV-Remote-Installation\runassvc.zip -Destination "C:\Users\kyr-aha\Downloads\ClamAV-Remote-Installation"

# All this just to unzip a file!
Add-Type -AssemblyName System.IO.Compression.FileSystem
function Unzip
{
    param([string]$zipfile, [string]$outpath)

    [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
}

Unzip "C:\Users\kyr-aha\Downloads\ClamAV-Remote-Installation\runassvc.zip" "C:\windows\temp"

Invoke-Expression "C:\Windows\Temp\runassvc\runassvc.exe"

#Get-Command -module Microsoft.PowerShell.Management *service*
#Get-Service | Where Status -EQ "Running" | Out-GridView
#Get-Service | Where Status -EQ "Stopped" | Out-GridView
#Get-Service | Where-Object {$_.Status -eq "Running"} | Out-File -filepath "$Env:userprofile\Desktop\ServicesList.txt"
<# Remove service if needed #>
#Stop-Service -Name "Clamd"

<# Start new service #>
#New-Service -Name "Clamd" -BinaryPathName "C:\Program Files\ClamAV\clamd" -DisplayName "clamd" -StartupType Automatic -Description "ClamAV Daemon"

#Get-Command -module Microsoft.PowerShell.Management *service*
Get-Service | Where Status -EQ "Running" | Out-GridView
Get-Service | Where Status -EQ "Stopped" | Out-GridView
Get-Service | Where-Object {$_.Status -eq "Running"} | Out-File -filepath "$Env:userprofile\Desktop\ServicesList.txt"

# Remove service if needed
#Stop-Service -Name "Clamd"

# Start new service
#New-Service -Name "Clamd" -BinaryPathName "C:\Program Files\ClamAV\clamd" -DisplayName "clamd" -StartupType Automatic -Description "ClamAV Daemon"

# Start clamd
#Start-Service -Name "Clamd"
